
if [[ -n $DOCKER_TLS_CERTDIR ]] ; then
    echo "configuring docker client for buildx"
    _CTXT="$( echo $RANDOM | md5sum | head -c 8 )"
    docker context create $_CTXT
    docker buildx create --use $_CTXT
    unset _CTXT
fi

