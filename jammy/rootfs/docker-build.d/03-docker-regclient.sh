
apt_get install git

git clone https://github.com/regclient/regclient.git
cd regclient
git checkout $REGCLIENT_TAG
make bin/regctl plugin-user
mv bin/regctl /usr/local/bin
cd ..

apt_get purge git

