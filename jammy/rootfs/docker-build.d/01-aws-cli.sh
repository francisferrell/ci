
apt_get install unzip

if [[ $ARCH == arm64 ]] ; then
    curl -sL 'https://awscli.amazonaws.com/awscli-exe-linux-aarch64.zip' --output ./awscliv2.zip
elif [[ $ARCH == amd64 ]] ; then
    curl -sL 'https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip' --output ./awscliv2.zip
else
    echo "unsupported architecture: $ARCH"
    exit 1
fi

unzip -q ./awscliv2.zip
./aws/install

apt_get purge unzip

