# regclient uses a go feature introduced in 1.16
# Ubuntu 20.04 provides 1.13
# sad Francis ):
# Ubuntu 22.04 provides 1.17, so this can be an apt install after upgrading to jammy
# happy Francis (:

mkdir golang
cd golang
curl -sLf "https://go.dev/dl/go1.17.7.linux-${ARCH}.tar.gz" --output ./go1.17.7.linux.tar.gz
tar -xf go1.17.7.linux.tar.gz
export GOROOT=$(pwd)/go
cd ..

mkdir gopath
export GOPATH=$(pwd)/gopath
export PATH=$GOROOT/bin:$PATH

