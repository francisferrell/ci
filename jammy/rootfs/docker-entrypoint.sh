#!/bin/bash

function _enable_credentials_helper() {
    local host="$1"
    local helper="$2"
    local docker_config="$( cat ~/.docker/config.json )"
    echo "$docker_config" | jq --arg host "$host" --arg helper "$helper" '.credHelpers[$host] = $helper' >~/.docker/config.json
    echo "enabled automatic docker login for ${host}"
}

for part in $( find /docker-entrypoint.d -type f -name '*.sh' | sort ) ; do
    source "$part"
done

exec "$@"

