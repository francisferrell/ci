#!/bin/bash

set -euxo pipefail

function apt_get() {
    local no_recommends=''

    if [[ $1 == install ]] ; then
        no_recommends=--no-install-recommends
    fi

    DEBIAN_FRONTEND=noninteractive apt-get \
        -o DPkg::Lock::Timeout=60 \
        -o Dpkg::Options::="--force-confnew" \
        "$@" \
        $no_recommends \
        --yes \
        ;
}


ARCH="$( dpkg --print-architecture )"
SCRATCH="$( mktemp --directory --tmpdir=/tmp docker-build.XXXXXXXXXX )"
cd "$SCRATCH"
apt_get update

for part in $( find /docker-build.d -type f -name '*.sh' | sort ) ; do
    source "$part"
done

cd /
apt_get clean
apt_get autoremove
rm -rf /var/lib/apt/lists/* "$SCRATCH"

