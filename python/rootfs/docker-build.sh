#!/bin/bash

set -euxo pipefail

function apt_get() {
    local no_recommends=''

    if [[ $1 == install ]] ; then
        no_recommends=--no-install-recommends
    fi

    DEBIAN_FRONTEND=noninteractive apt-get \
        -o DPkg::Lock::Timeout=60 \
        -o Dpkg::Options::="--force-confnew" \
        "$@" \
        $no_recommends \
        --yes \
        ;
}


SCRATCH="$( mktemp --directory --tmpdir=/tmp docker-build.XXXXXXXXXX )"
cd "$SCRATCH"
apt_get update



# depndency list taken from https://devguide.python.org/setup/#linux
apt_get install -y \
    gdb lcov pkg-config \
    libbz2-dev libffi-dev libgdbm-dev libgdbm-compat-dev liblzma-dev \
    libncurses5-dev libreadline6-dev libsqlite3-dev libssl-dev \
    lzma lzma-dev tk-dev uuid-dev zlib1g-dev \
    git

git clone https://github.com/pyenv/pyenv.git ~/.pyenv
cd ~/.pyenv
git checkout $( git tag | grep '^v2\.' | sort --version-sort -r | head -n 1 )
cd -

export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init --path)"
eval "$(pyenv init -)"

pyenv install $PYTHON_VERSION
pyenv global $PYTHON_VERSION

apt_get purge -y \
    gdb lcov pkg-config \
    libbz2-dev libffi-dev libgdbm-dev libgdbm-compat-dev liblzma-dev \
    libncurses5-dev libreadline6-dev libsqlite3-dev libssl-dev \
    lzma lzma-dev tk-dev uuid-dev zlib1g-dev \
    git

pip3 install -U pip wheel
pip3 install \
    pylint \
    pytest \
    pytest-cov \
    ;



cd /
apt_get clean
apt_get autoremove
rm -rf /var/lib/apt/lists/* "$SCRATCH"

