
# Features

This image is based on Ubuntu LTS and is intended to be a swiss army knife for CI jobs.


## Weekly Builds

This image is automatically rebuilt weekly to get the latest OS patches and tool versions.


## Common Tools

This image provides the following Ubuntu packages:

- build-essential
- curl
- gnupg
- jq
- make


## AWS CLI

This image provides the latest AWS CLI v2.


## Python 3

This image provides the latest python 3 provided by Ubuntu LTS. This includes the development
headers for building compiled extensions as well as the `venv` standard library module.


## Docker in Docker

This image provides the `docker` CLI client and an environment that works alongside the `:dind`
docker image. The user needs only to provide the standard environment variables recognized by the
`docker` docker image, such as `DOCKER_TLS_CERTDIR`, for it to configure itself correctly and find
its docker host.

For example, when using this image for a Gitlab CI job, including the following yml will enable
docker-in-docker.

```yml
  services:
    - docker:24-dind
  variables:
    DOCKER_TLS_CERTDIR: "/certs"
```


## Docker Credential Helpers

Included are a variety of docker credential helpers, which automate the `docker login` process
based on credentials available in environment variables. In short, you don't have to explicitly
call `docker login ...` if you have provided the necessary environment variables.

You can still perform an explicit `docker login` if your use case doesn't fit those covered by
the helpers described below.


### AWS ECR

The AWS ECR credential helper requires the following environment variables:

- `AWS_ECR` -- the URI to the AWS ECR repository or registry hostname

It can be a fully qualified docker image URI, just the hostname provided by your AWS account for
ECR, or any ECR you have IAM permission to access.

You may need additional environment variables to provide credentials to the AWS SDK, but not in all
scenarios. For example, the AWS SDK can get its credentials from an IAM role when running in an AWS
compute environment. You can provide any environment variable that is recognized by the AWS CLI v2,
which is what the credential helper uses under the hood.

Typically, in the absence of an IAM role, you'll need the following environment variables:

- `AWS_ACCESS_KEY_ID`
- `AWS_SECRET_ACCESS_KEY`
- `AWS_DEFAULT_REGION`

The AWS SDK will need IAM permissions as follows:

- `ecr:GetAuthorizationToken` on resource `*`

For pulling an image, these are the minimum IAM permissions required. All of them can be scoped to
just the ARN of the repository in question.

- `ecr:BatchCheckLayerAvailability`
- `ecr:BatchGetImage`
- `ecr:GetDownloadUrlForLayer`

For pushing an image, these IAM permissions will need to be added. Again, all of them can be scoped
to just the ARN of the repository in question.

- `ecr:CompleteLayerUpload`
- `ecr:InitiateLayerUpload`
- `ecr:PutImage`
- `ecr:UploadLayerPart`


### Docker Hub

The Docker Hub credential helper requires the following environment variables:

- `DOCKER_HUB_USERNAME` -- your docker hub username
- `DOCKER_HUB_ACCESS_TOKEN` -- your docker hub personal access token


### Gitlab

The Gitlab container registry credential helper requires the following environment variables:

- `CI_REGISTRY`
- `CI_REGISTRY_USER`
- `CI_REGISTRY_PASSWORD`

This differs from the other helpers in that these environment variables are automatically made
available by the Gitlab CI runtime. You won't ever need to set them unless you want to access a
Gitlab container registry in a runtime environment that is not Gitlab CI.


## Regctl

This image provides the `regctl` tool from the regclient project. For more details, see
https://github.com/regclient/regclient.


# Links

- Source: https://gitlab.com/francisferrell/ci
- Docker Hub: https://hub.docker.com/r/francisferrell/ci

